### Yêu cầu nhận được
- Thêm logger, bind... cho hủy giao dịch cho branch [hình](https://gitlab.com/vinaas/Tasks/uploads/cba301d4dbf39ac43c26e832f1592df3/Capture.PNG)
- Giao diện [hình](https://gitlab.com/vinaas/Tasks/uploads/2a10edcb43670748347a3527172112d2/c008ad1e-0f38-40d5-aeac-8442cbe30509.png)
- Viết code cho file huy-giao-dich.ts
 - code [linkcommit](https://gitlab.com/kieuhoi/kieuhoi-spa/commit/e018fd2ed8cfc990d2442c9cfae68588f63225fd)
 - Kết quả [hình](https://gitlab.com/vinaas/Tasks/uploads/0d800bc08e24ff46b69d8f5e2f479c9d/image.png)
- Thêm button hủy bỏ
- Sửa cho GridView hiện ra
- Thêm logger cho nút Yêu cầu hủy

### Đã thực hiện
- Thêm logger, bind... cho hủy giao dịch cho branch [hình](https://gitlab.com/vinaas/Tasks/uploads/42120509578e1ec2da384a5a099e5f0e/1c72defe-95a2-4cfe-b192-5db1f6716e2d.png)
- Giao diện [hình](https://gitlab.com/vinaas/Tasks/uploads/2a10edcb43670748347a3527172112d2/c008ad1e-0f38-40d5-aeac-8442cbe30509.png)
- Viết code cho file huy-giao-dich.ts
- Thêm button hủy bỏ
- Đã sửa gridview cho tab hủy giao dịch và duyệt nhưng không có dữ liệu hiển thị ra
 - code commit [link] (https://gitlab.com/kieuhoi/kieuhoi-spa/commit/c41ff2184e0652d7269847d275903c244fc8bfd7)
 - Kết quả [hình](https://gitlab.com/vinaas/Tasks/uploads/26b417a9ab27134262606ae5d356ba57/image.png)

### Đang thực hiện
- Hiện 2 nút Xem, Yêu cầu hủy ở gridview
- Hiện dialog khi bấm xem
